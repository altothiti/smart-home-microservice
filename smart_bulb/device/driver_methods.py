import aioredis
import asyncio
from django.conf import settings

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
loop = asyncio.get_event_loop()
pub = loop.run_until_complete(aioredis.create_redis(settings.REDIS_HOST))
loop.run_until_complete(pub.auth('ictadmin'))


async def turn_off(tag_value):
    value = 0
    publish_tag = f"settag:TestSystem.{tag_value}.onoff"

    pub.publish(publish_tag, value)


async def turn_on(tag_value):
    value = 1
    publish_tag = f"settag:TestSystem.{tag_value}.onoff"

    pub.publish(publish_tag, value)


async def change_color(tag_value, color_code):
    color_code = int(color_code, 0)

    publish_tag = f"settag:TestSystem.{tag_value}.color"

    pub.publish(publish_tag, color_code)


async def adjust_brightness(tag_value, brightness):
    publish_tag = f"settag:TestSystem.{tag_value}.brightness"

    pub.publish(publish_tag, brightness)
