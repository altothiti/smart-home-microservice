
def get_all_light_bulb_devices(devices):
    return list(filter(filter_light_bulb_devices, devices))


def get_light_bulb_device(devices, light_bulb_name):
    return list(filter(lambda device: filter_light_bulb_device(device, light_bulb_name), devices))


def get_light_bulb_details(light_bulb_devices):
    return list(map(map_light_bulb_details, light_bulb_devices))


def get_light_bulb_device_tags(tags, light_bulb_devices):
    result = [{"id": light_bulb_device["id"], "name": light_bulb_device["name"], "tags": [
    ]} for light_bulb_device in light_bulb_devices]

    light_bulb_device_tags = {
        str(light_bulb_device["id"]): [] for light_bulb_device in light_bulb_devices}

    for tag in tags:
        tag_device = str(tag["device"])

        if tag_device not in light_bulb_device_tags:
            continue

        light_bulb_device_tags[tag_device].append(tag)

    for light_bulb_device in result:
        device_id = str(light_bulb_device["id"])
        light_bulb_device["tags"] = light_bulb_device_tags[device_id]

    return result


def filter_light_bulb_devices(device):
    port = device["port"]
    port_name = port["name"]

    return True if port_name == "new_hue" else False


def filter_light_bulb_device(device, light_bulb_name):
    port = device["port"]
    port_name = port["name"]
    name = device["name"]

    return True if name == light_bulb_name and port_name == "new_hue" else False


def map_light_bulb_details(light_bulb_device):
    id = light_bulb_device["id"]
    port = light_bulb_device["port"]
    server_ip_address = port["detail"]["address"]
    name = light_bulb_device["name"]

    return {'id': id, 'ip_address': server_ip_address, "name": name}
