from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .driver_methods import turn_off, turn_on, change_color, adjust_brightness
from .methods import (get_all_light_bulb_devices, get_light_bulb_details,
                      get_light_bulb_device, get_light_bulb_device_tags)
import urllib3
import asyncio
import json
import os


class AllLightBulbDevicesView(APIView):
    def get(self, request):
        data = get_all_devices()
        result = data["results"]
        light_bulb_devices = get_all_light_bulb_devices(result)
        response = get_light_bulb_details(light_bulb_devices)

        return Response(response, status=status.HTTP_202_ACCEPTED)


class LightBulbDeviceView(APIView):
    def get(self, request, light_bulb_name):
        data = get_all_devices()
        result = data["results"]
        light_bulb_device = get_light_bulb_device(result, light_bulb_name)
        response = get_light_bulb_details(light_bulb_device)

        return Response(response, status=status.HTTP_202_ACCEPTED)


class AllLightBulbDeviceTagsView(APIView):
    def get(self, request):
        data = get_all_devices()
        result = data["results"]
        light_bulb_devices = get_all_light_bulb_devices(result)
        data = get_all_tags()
        result = data["results"]
        response = get_light_bulb_device_tags(
            result, light_bulb_devices)

        return Response(response, status=status.HTTP_202_ACCEPTED)


class LightBulbDeviceTagView(APIView):
    def get(self, request, light_bulb_name):
        data = get_all_devices()
        result = data["results"]
        light_bulb_device = get_light_bulb_device(result, light_bulb_name)
        data = get_all_tags()
        result = data["results"]
        response = get_light_bulb_device_tags(
            result, light_bulb_device)

        return Response(response, status=status.HTTP_202_ACCEPTED)


class TurnOffView(APIView):
    def post(self, request, tag_value):
        loop = create_event_loop()
        loop.run_until_complete(turn_off(tag_value))

        return Response(status=status.HTTP_202_ACCEPTED)


class TurnOnView(APIView):
    def post(self, request, tag_value):
        loop = create_event_loop()
        loop.run_until_complete(turn_on(tag_value))

        return Response(status=status.HTTP_202_ACCEPTED)


class ChangeColorView(APIView):
    def post(self, request, tag_value):
        data = request.data
        color_code = data["color_code"]

        loop = create_event_loop()
        loop.run_until_complete(change_color(tag_value, color_code))

        return Response(status=status.HTTP_202_ACCEPTED)


class AdjustBrightnessView(APIView):
    def post(self, request, tag_value):
        data = request.data
        brightness = data["brightness"]

        loop = create_event_loop()
        loop.run_until_complete(adjust_brightness(tag_value, brightness))

        return Response(status=status.HTTP_202_ACCEPTED)


def create_event_loop():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop = asyncio.get_event_loop()

    return loop


def get_all_devices():
    # json_file = open(os.getcwd() + '\\test_data\\responseAllDevice.json')
    # data = json.load(json_file)

    http = urllib3.PoolManager(cert_reqs='CERT_NONE', assert_hostname=False)
    header = {"authorization": "Token f2b5c1c068c610e3b09ca32f34fb7c14f252fc1f"}
    response = http.request(
        'GET', 'https://192.168.2.152/restapi/tag/device/', headers=header)
    data = json.loads(response.data.decode('utf-8'))

    return data


def get_all_tags():
    # json_file = open(os.getcwd() + '\\test_data\\responseAllTags.json')
    # data = json.load(json_file)

    http = urllib3.PoolManager(cert_reqs='CERT_NONE', assert_hostname=False)
    header = {"authorization": "Token f2b5c1c068c610e3b09ca32f34fb7c14f252fc1f"}
    response = http.request(
        'GET', 'https://192.168.2.152/restapi/tag/tag/', headers=header)
    data = json.loads(response.data.decode('utf-8'))

    return data
