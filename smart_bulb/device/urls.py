from django.urls import path
from .views import (AllLightBulbDevicesView, LightBulbDeviceView,
                    TurnOffView, TurnOnView, ChangeColorView, AdjustBrightnessView,
                    AllLightBulbDeviceTagsView, LightBulbDeviceTagView)

urlpatterns = [
    path('', AllLightBulbDevicesView.as_view()),
    path('tag/', AllLightBulbDeviceTagsView.as_view()),
    path('tag/<slug:light_bulb_name>/', LightBulbDeviceTagView.as_view()),
    path('<slug:light_bulb_name>/', LightBulbDeviceView.as_view()),
    path('off/<slug:tag_value>/', TurnOffView.as_view()),
    path('on/<slug:tag_value>/', TurnOnView.as_view()),
    path('color/<slug:tag_value>/', ChangeColorView.as_view()),
    path('brightness/<slug:tag_value>/',
         AdjustBrightnessView.as_view())
]
