FROM python:3.6

WORKDIR /usr/src/app

COPY . .

RUN pip install -r requirements.txt

WORKDIR /usr/src/app/smart_bulb

EXPOSE 8000